#include <iostream>

using namespace std;

//Origin at lower-left
class Rect
{
public:
	int xPos;
	int yPos;
	int width;
	int height;
	float anchorPoint;
	Rect(int p_x, int p_y, int p_width, int p_height, float p_anchorPoint);

	bool didIntersectRect(Rect p_recToCheck);
};

Rect::Rect(int p_x, int p_y, int p_width, int p_height, float p_anchorPoint)
{
	xPos = p_x;
	yPos = p_y;
	width = p_width;
	height = p_height;
	anchorPoint = p_anchorPoint;

}

bool Rect::didIntersectRect(Rect p_recToCheck)
{
	bool l_result = false;

	bool l_xIntersect = false;
	bool l_yIntersect = false;

	int anchorModifier = 1;

	if (anchorPoint != 1)
	{
		anchorModifier = anchorPoint;
	}

	//get left point
	//check if intersects x-axis

	if (xPos >= (p_recToCheck.xPos * anchorModifier) && xPos <= p_recToCheck.xPos + (p_recToCheck.width * anchorModifier))
	{

		l_xIntersect = true;
	}

	else if (xPos + width <= (p_recToCheck.xPos * anchorModifier) + p_recToCheck.width && xPos + width <= (p_recToCheck.xPos + p_recToCheck.width * anchorModifier))
	{
		l_xIntersect = true;
	}

	//check if intersects y-axis
	if (yPos >= (p_recToCheck.yPos * anchorModifier) && yPos <= (p_recToCheck.yPos + p_recToCheck.height * anchorModifier))
	{
		l_yIntersect = true;
	}

	else if (yPos + height <= (p_recToCheck.yPos + p_recToCheck.height * anchorModifier) && yPos + height <= (p_recToCheck.yPos + p_recToCheck.height * anchorModifier))
	{
		l_yIntersect = true;
	}

	l_result = l_xIntersect && l_yIntersect;


	return l_result;
}


int main()
{
	int aValue = 0;

	cout << "Enter Anchor Point value" << endl;
	cin >> aValue;



	Rect rectA = Rect(0, 0, 10, 10, aValue);
	Rect rectB = Rect(5, 5, 15, 15, aValue);

	if (rectA.didIntersectRect(rectB))
	{
		cout << "Rect A and B intersect" << endl;
	}

	else
	{
		cout << "Rect A and B did not intersect" << endl;
	}

	system("pause");
	return 0;
}


