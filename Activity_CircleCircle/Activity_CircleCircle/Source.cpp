#include <iostream>
#include <Math.h>
using namespace std;

class Circle
{
public:
	int xPos;
	int yPos;
	int radius;
	Circle(int p_x, int p_y, int p_radius);

	bool didIntersectCirc(Circle p_recToCheck, Circle p_circToCheckB, int distance);
};

Circle::Circle(int p_x, int p_y, int p_radius)
{
	xPos = p_x;
	yPos = p_y;
	radius = p_radius;

}

int getDistance(Circle circA, Circle circB)
{
	float a = circA.xPos - circB.xPos; //x positions
	float b = circA.yPos - circB.yPos; // y positions


	float c = sqrt((a*a) + (b*b)); // Hypotenus
	return c;
}

bool Circle::didIntersectCirc(Circle p_circToCheckA, Circle p_circToCheckB, int distance)
{
	bool l_result = false;

	if (distance <= (p_circToCheckA.radius + p_circToCheckB.radius))
	{

		l_result = true;
	}

	return l_result;
}


int main()
{

	Circle circA = Circle(5, -1, 10);
	Circle circB = Circle(8, -5, 6);
	
	//Get Hypotenus
	int distance = getDistance(circA, circB);

	cout << "Hypotenus: " << distance << endl;
	cout << "Sum of Radii: " << circA.radius + circB.radius << endl << endl;

	if (circA.didIntersectCirc(circA, circB, distance))
	{
		cout << "Circle A and B intersect" << endl;
	}

	else
	{
		cout << "Circle A and B did not intersect" << endl;
	}

	system("pause");
	return 0;
}


