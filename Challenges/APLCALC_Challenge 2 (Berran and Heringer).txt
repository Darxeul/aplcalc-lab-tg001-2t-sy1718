//Kyle Edward Berran
//Deshawn Heringer

// color vec3
vec3 black = vec3(0,0,0);
vec3 white = vec3(1,1,1);
vec3 red = vec3(1,0,0);
vec3 blue = vec3(0,0,1);
vec3 green = vec3(0,1,0);
vec3 yellow = vec3(1,1,0);
vec3 pink = vec3(1,0,1);
vec3 cyan = vec3(0,1,1);

// uv fx

vec2 createUV(vec2 fc)
{
    vec2 uv = fc/iResolution.xy;
	float ratio = iResolution.x/iResolution.y;
    uv.x *= ratio;
    return uv;
}
vec2 useUV(vec2 uv, vec2 pos, mat2 func)
{   
    // reuse base uv for reattachment
    
    uv -= pos; // send to origin
    
    uv *= func; // scale2D/rotate2D
    
    uv += pos; // send back to position
    
    return uv;
}

mat2 rotate2D(float angle)
{
    return mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
}

mat2 scale2D(vec2 scale, float ratio)
{
    return mat2(scale.x*ratio, 0, 0, scale.y*ratio);
}

// shape fx

float fillCircle(vec2 uv, vec2 pos, float r)
{
    float d = distance(pos, uv);
    return 1.-step(r,d);
}

float fillSquare(vec2 uv, vec3 points) 
{
    // x = xpos
    // y = ypos
    // z = range/2
	return
        (step(points.x+points.z/2., uv.x)-step(points.x-(points.z/2.), uv.x))*
        (step(points.y+points.z/2., uv.y)-step(points.y-(points.z/2.), uv.y));
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;
	float ratio = iResolution.x / iResolution.y;
    uv.x*=ratio;
    
    // Time varying pixel color
    vec3 col = black;
	
    float shape = 0.;
    vec2 shapePos = vec2(iTime*ratio, sin(iTime*5.)+2.5);
    
    for (float x = 0.; x < 6.0; x+=0.01)
    {
        float y = sin(x*5.)+2.5;
        shape = fillCircle(uv*5., vec2(x*ratio, y), 0.02);
        col = mix(col, white, shape);
        
    }
    
    float slope = shapePos.y/shapePos.x;
    float angle = atan(slope)-1./shapePos.y;
    
    uv = useUV(uv*5., shapePos, rotate2D(angle*2.));   
   	shape = fillSquare(uv, vec3(shapePos, 0.25));
    
   	col = mix(col, white, shape);
    
    // Output to screen
    fragColor = vec4(col,1.0);
}